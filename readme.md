# creditcounter

Ein kleines Programm, das beim Zusammenstellen des Studienplans hilft, indem es
zusammenzählt wieviele Credits in welchen Bereichen geplant sind.

## Beispiel

Wir beginnen zum Beispiel mit dieser Vorlage, die die Studienbereiche vorgibt:

```
(0c/180c)
	Fachstudium (0c/96c)
		Grundlagen der Informatik (0c/30c)

		Mathematische Grundlagen der Informatik (0c/36c)

		Kerninformatik (0c/30c)

	Professionalisierungsbereich (0c/72c)
		Schwerpunkt (0c/42c)

		Schlüsselkompetenzen (0c/20c)
```

Jetzt tragen wir einige Module und die zugehörigen Credits ein:

```
(0c/180c)
	Fachstudium (0c/96c)
		Grundlagen der Informatik (0c/30c)
			Informatik I (10c)
			Informatik II (10c)
			Informatik III (10c)

		Mathematische Grundlagen der Informatik (0c/36c)

		Kerninformatik (0c/30c)

	Professionalisierungsbereich (0c/72c)
		Schwerpunkt (0c/42c)

		Schlüsselkompetenzen (0c/20c)
```

Wenn wir das jetzt durch `creditcounter` pipen, werden die Creditsummen der
Bereiche aktualisiert:

```
(30c/180c)
	Fachstudium (30c/96c)
		Grundlagen der Informatik (30c/30c)
			Informatik I (10c)
			Informatik II (10c)
			Informatik III (10c)

		Mathematische Grundlagen der Informatik (0c/36c)

		Kerninformatik (0c/30c)

	Professionalisierungsbereich (0c/72c)
		Schwerpunkt (0c/42c)

		Schlüsselkompetenzen (0c/20c)
```

Am besten bearbeitet man seinen Plan in einem Editor, der Text durch externe
Programme pipen kann. In vim geht das zum Beispiel mit `:%!./creditcounter`.

## Eingabeformat

Ein Bereich ist eine Zeile, die einen Ausdruck enthält, auf den das Muster
\\([0-9]+c/ passt. Zu einem Bereich gehören alle folgenden Zeilen, die mit mehr
Tabs eingerückt sind als der Bereich.

Ein Modul ist eine Zeile, die einen Ausdruck enthält, auf den das Muster
\\([0-9]c\\) passt. Die Zahl in diesem Ausdruck ist die Anzahl der Credits.
